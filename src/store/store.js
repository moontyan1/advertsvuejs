import Vue from 'vue';
import Vuex from 'vuex';
import { auth } from '../DAL/requests';
import { advertsAPI } from '../DAL/requests'
import { PHOTOS_URL } from '../constants'

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    user: JSON.parse(localStorage.getItem('user')) || {
      id: null,
      username: null,
      role: null,
      token: null,
    },
    photoUrl: '',
    error: ''
  },
  actions: {
    initializePhoto(context, username) {
      advertsAPI.getPhotoUrl(username).then(response => {
        const photoUrl = response.data.result.photoPath
        context.commit('initializePhoto', photoUrl)
      })
    },
    fileUpload(context, file) {
      let formData = new FormData()
      formData.append('file', file)
      advertsAPI.uploadAvatar(formData).then(response => {
        const photoUrl = PHOTOS_URL + response.data.result.photoURL
        context.commit('fileUpload', photoUrl)
      })
    },
    createUser(context, credentials) {
      return new Promise((resolve, reject) => {
        auth.signUp(credentials.email, credentials.username, credentials.password).then((response) => {
          const user = {
            id: response.data.newUser.userCredentials.id,
            username: response.data.newUser.userCredentials.username,
            role: response.data.newUser.userCredentials.role,
            token: response.data.newUser.accessToken,
          };

          localStorage.setItem('user', JSON.stringify(user));
          context.commit('createUser', user);
          resolve(response);
        }).catch((error) => {
          console.log(error);
          reject(error);
        });
      });
    },
    getUser(context, credentials) {
      const encodedHeader = Buffer.from(`${credentials.email}:${credentials.password}`).toString('base64');
      return new Promise((resolve, reject) => {
        auth.login(encodedHeader).then((response) => {
          const user = {
            id: response.data.user.userCredentials.id,
            username: response.data.user.userCredentials.name,
            role: response.data.user.userCredentials.role,
            token: response.data.user.accessToken,
          };

          localStorage.setItem('user', JSON.stringify(user));
          context.commit('getUser', user);
          context.commit('setError', '')
          resolve(response);
        }).catch((error) => {
          console.log(error);
          context.commit('setError', 'Invalid email or password!')
          reject(error);
        });
      });
    },
    logout(context) {
      if (context.getters.loggedIn) {
        return new Promise((resolve, reject) => {
          localStorage.removeItem('user');
          context.commit('logout');
          resolve();
        }).catch((error) => {
          localStorage.removeItem('user');
          context.commit('logout');
        });
      }
    },
  },
  mutations: {
    setError(state, error) {
      state.error = error
    },
    initializePhoto (state, photoUrl) {
      state.photoUrl = photoUrl
    },
    fileUpload(state, photoUrl) {
      state.photoUrl = photoUrl
    },
    createUser(state, user) {
      state.user = user;
    },
    getUser(state, user) {
      state.user = user;
    },
    logout(state) {
      state.user = {
        id: null,
        username: null,
        role: null,
        token: null,

      };
    },
  },

  getters: {
    loggedIn(state) {
      return state.user.token !== null;
    },
    loggedUser(state) {
      return state.user.username;
    },
    loggedUserAvatar(state) {
      return state.photoUrl
    },
    loggedUserRole(state) {
      return state.user.role
    },
    getError(state) {
      return state.error
    }
  },
});
