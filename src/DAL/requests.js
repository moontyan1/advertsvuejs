import Axios from 'axios';
import { API_URL } from '../constants';

const axiosInstance = Axios.create({
  baseURL: API_URL,
});

export const advertsAPI = {
  getAdverts(page, limit) {
    return axiosInstance.get(`adverts?page=${page}&limit=${limit}`).then((response) => response.data);
  },
  getCategories() {
    return axiosInstance.get('category').then((response) => response.data);
  },
  getSortedAdverts(category, page) {
    return axiosInstance.get(`adverts/category/${category}?page=${page}`).then((response) => response.data);
  },
  getBySearch(term, page) {
    return axiosInstance.get(`search?advert=${term}?page=${page}`).then((response) => response.data);
  },
  postAdvert(title, body, category) {
    return axiosInstance.post('adverts', { title, body, category }, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`,
      },
    });
  },
  getPhotoUrl(username) {
    return axiosInstance.get(`profilePicture?username=${username}`);
  },
  getUserAdverts(username) {
    return axiosInstance.get(`adverts/author/${username}`);
  },
  getAdvertById(id) {
    return axiosInstance.get(`adverts/${id}`);
  },
  deleteAdvert(id) {
    return axiosInstance.delete(`adverts/${id}`, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`,
      },
    });
  },
  editAdvert(id, title, body, category) {
    return axiosInstance.put(`adverts/${id}`, { title, body, category }, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`
      },
    });
  },
  uploadAvatar(formData) {
    return axiosInstance.put('upload', formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
         Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`
      }
    })
  },
  addComment(id, body) {
    return axiosInstance.put('comment', {id, body}, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`
      }
    })
  },
  deleteComment(advertId, commentId) {
    return axiosInstance.put(`delete/${commentId}`, { advertId }, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`
      }
    })
  },
  addInfo(firstName, lastName, bio, contacts) {
    return axiosInstance.put('user', { firstName, lastName, bio, contacts }, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`
      }
    })
  },
  getInfo(username) {
    return axiosInstance.get(`user/${username}`)
  },
  banUser(user) {
    return axiosInstance.put('ban', { user }, {
      headers: {
        Authorization: `Bearer ${JSON.parse(localStorage.getItem('user')).token}`
      }
    })
  }
}

export const auth = {
  login(encodedHeader) {
    return axiosInstance.post('login', {}, {
      headers: {
        Authorization: `Basic ${encodedHeader}`,
      },
    });
  },
  signUp(email, username, password) {
    return axiosInstance.post('register', { email, username, password });
  },
};
